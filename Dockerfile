# Use an official lightweight Nginx image
FROM nginx:alpine

# Set the working directory to the default Nginx public directory
WORKDIR /usr/share/nginx/html

# Copy all files from the current directory to the working directory
COPY . .

# Expose port 80
EXPOSE 80

# Command to run Nginx in the foreground
CMD ["nginx", "-g", "daemon off;"]
