# This is a demo web application that we will be using in combination with Minikube & Kaniko to make the repo into a container, push that container to the gitlab container registry, and finally deploy that container locally on a kubernetes cluster.

## Step 1. Install minikube, kubectl & docker to your ubuntu VM. 
REF https://www.linuxbuzz.com/install-minikube-on-ubuntu/

Adding dependencies and installing docker
```
sudo apt update && sudo apt upgrade
sudo reboot
sudo apt install ca-certificates curl gnupg wget apt-transport-https -y
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo usermod -aG docker $USER
newgrp docker
```

Adding repo and installing minikube
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
minikube version
```
Installing kubectl
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/
kubectl version -o yaml 
```

## Step 2. Create a minikube cluster.
```
minikube start --driver=docker
```

## Step 3. Create a Gitlab free account, and create a private group and new project, mine will be called kaniko-demo
1. Create a gitlab account with a free no-card trial.
2. Create a new group named kaniko and make sure it's private.
3. Create a new project and name it kaniko-demo so the URL's will be the same.

## Step 4. Create a personal deploy token to authenticate with your new group and project.
Profile -> Edit Profile -> Access Tokens
1. Add Token
2. Name = kaniko
3. Check read_repository,write_repository,read_registry,write_registry
4. Create personal access token
5. Copy password to be used in step 5 and save it because it is gone after you leave the page.

## Step 5. Create CI/CD variables for kaniko 
1. Go to your project -> Settings -> CI/CD
2. Open Variables, and click Add Variable ( Not a group variable )
3. Create a variable with key=CI_REGISTRY_USER, value = kaniko
4. Create a variable with key=CI_REGISTRY_PASSWORD=(Copied Password from Step 4.5)
5. Create a variable with key=CI_REGISTRY, value=https://registry.gitlab.com

## Step 6. Clone the template web-app repo and push to your new repo.
1. Clone the repo and push to your new project
```
git clone https://gitlab.com/demo7513849/web-app.git
cd web-app/
git remote add origin-new https://gitlab.com/kaniko/kaniko-demo.git #Adjust as needed 
git branch -M push-web-app
git push -uf origin-new push-web-app
```
2. Go to your project page, click "Create Merge Request", then scroll down and click "Create Merge Request"

3. You'll see a merge conflict, click resolve merge conflict, and you'll see both README.md's . Click "Use ours" on the top right of the first README with the tutorial instructions, then scroll down and click "Comit to source branch"

4. Refresh the page then Find and click Merge.

## Step 7. Create the gitlab runner
1. Install helm locally #REF https://helm.sh/docs/intro/install/
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
2. Install gitlab repo
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
```
3. Create a runner-token from the project by going to Project -> CI/CD -> Runners -> New Project Runner -> Check "Run Untagged jobs" -> Create Runner -> Copy the Runner authentication token to put in step 4. 

4. Create a values file for gitlab-runner
Edit "TOKEN" with the token copied in Step 7.3
```
cat <<EOF > gitlab-runner-values.yml
# gitlab-runner-values.yml

gitlabUrl: https://gitlab.com/
runnerToken: "TOKEN"
unregisterRunners: true
EOF
```
5. Install gitlab-runner using helm
```
helm install gitlab-runner gitlab/gitlab-runner -f gitlab-runner-values.yml
```
6. Check that runner was authenticated by going to Project -> CI/CD -> Runners -> Look for assigned project runners

7. Uncheck the "Enable shared runners for this project" so the project will use our kubernetes runner.

## Step 8. Create and apply a role and role-binding for the service account 'default' that gitlab-runner will use
1. Run this command
```
cat <<EOF > gitlab-runner-rbac.yml
# gitlab-runner-rbac.yml
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: default-runner
  namespace: default
rules:
  - verbs:
      - '*'
    apiGroups:
      - '*'
    resources:
      - '*'
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: default-runner-binding
  namespace: default
subjects:
  - kind: ServiceAccount
    name: default
    namespace: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: default-runner
---
EOF
```
2. Apply the file to create the roles and role-bindings
```
kubectl apply -f gitlab-runner-rbac.yml
```

## Step 9. Create the .gitlab-ci.yml file with a default kaniko template.
1. Go to the project repo and click create file with the name ".gitlab-ci.yml" and paste this template. 
```
image: 
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables: 
  #More Information on Kaniko Caching: https://cloud.google.com/build/docs/kaniko-cache
  KANIKO_CACHE_ARGS: "--cache=true --cache-copy-layers=true --cache-ttl=24h"
  VERSIONLABELMETHOD: "OnlyIfThisCommitHasVersion" # options: "OnlyIfThisCommitHasVersion","LastVersionTagInGit"
  IMAGE_LABELS: >
    --label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.revision=$CI_COMMIT_SHA
    --label org.opencontainers.image.source=$CI_PROJECT_URL
    --label org.opencontainers.image.documentation=$CI_PROJECT_URL
    --label org.opencontainers.image.licenses=$CI_PROJECT_URL
    --label org.opencontainers.image.url=$CI_PROJECT_URL
    --label vcs-url=$CI_PROJECT_URL
    --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
    --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
    --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
    --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
    --label com.gitlab.ci.cijoburl=$CI_JOB_URL
    --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID
  DOCKERFILE: 'Dockerfile'
  CI_IMAGE_NAME: registry.gitlab.com/$CI_PROJECT_PATH

get-latest-git-version:
  stage: .pre
  image: 
    name: alpine/git
    entrypoint: [""]
  rules:
    - if: '$VERSIONLABELMETHOD == "LastVersionTagInGit"'    
  script:
    - |
      echo "the google kaniko container does not have git and does not have a packge manager to install it"
      git clone https://github.com/GoogleContainerTools/kaniko.git
      cd kaniko
      echo "$(git describe --abbrev=0 --tags)" > ../VERSIONTAG.txt
      echo "VERSIONTAG.txt contains $(cat ../VERSIONTAG.txt)"
  artifacts:
    paths:
      - VERSIONTAG.txt


.build_with_kaniko:
  #Hidden job to use as an "extends" template
  stage: build
  script:
    - | 
      echo "Building and shipping image to $CI_IMAGE_NAME"
      #Build date for opencontainers
      BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
      #Description for opencontainers
      BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
      #Add ref.name for opencontainers
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CI_IMAGE_NAME:$CI_COMMIT_REF_NAME"

      #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
      if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then VERSIONLABEL=$(cat VERSIONTAG.txt); fi
      if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then VERSIONLABEL=$CI_COMMIT_TAG; fi
      if [[ ! -z "$VERSIONLABEL" ]]; then 
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"
      fi
      
      ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"; fi
      if [[ -n "$ADDITIONALTAGLIST" ]]; then 
        for TAG in $ADDITIONALTAGLIST; do 
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_IMAGE_NAME:$TAG "; 
        done; 
      fi
      
      #Reformat Docker tags to kaniko's --destination argument:
      FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g) 

      echo "Kaniko arguments to run: --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/$DOCKERFILE $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS"
      mkdir -p /kaniko/.docker
      echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)\"}}}" > /kaniko/.docker/config.json
      /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/$DOCKERFILE $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS


build-for-gitlab-project-registry:
  extends: .build_with_kaniko
```
2. Once you save this file, it will trigger a pipeline that is looking for a file named .gitlab-ci.yml that you are creating, so everytime a repo is updated or pushed, the runner will run the pipeline.

3. Go to Project -> Build -> Pipelines and find the pipeline that is running and confirm it ran successfully and view its logs.

4. Confirm the container was pushed to the registry by going to Project -> Deploy -> Container Registry

## Step 10. Create a deployment on kubernetes to pull the image and create a pod and service to view the website. Kubernetes needs authentication access with the container registry source so we need to create a secret it can use to connect to gitlab's registry to retrieve our container.
1. Get the base64 conversion of your personal token username:password for the auth used in Step 10.2.
```
echo -n "kaniko:PASSWORD" | base64
```

2. Create the json dockerconfig.json file that is the template for the secret and edit with your token password and auth based64 conversion in Step 10.1
```
cat <<EOF > dockerconfig.json
{
    "auths": {
        "https://registry.gitlab.com": {
            "username": "kaniko",
            "password": "REDACTED",
            "auth": "REDACTED"
        }
    }
}
EOF
```
3. Create the secret based on that file that will have the name of registry-creds , the key name of .dockerconfigjson ( required ), and value of the content in the file we just created in 10.2
```
kubectl create secret generic registry-creds --from-file=.dockerconfigjson=dockerconfig.json --type=kubernetes.io/dockerconfigjson
```

4. Create the deployment and service objects by creating the deployment.yaml file by copy and pasting this command in your terminal.
```
cat <<EOF > deployment.yaml
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: kaniko-deployment
  namespace: default
  labels:
    app: kaniko-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kaniko-deployment
  template:
    metadata:
      labels:
        app: kaniko-deployment
    spec:
      containers:
        - name: node-alpine
          image: registry.gitlab.com/kaniko/kaniko-demo:main
          ports:
            - name: web
              containerPort: 80
              protocol: TCP
          imagePullPolicy: Always
      restartPolicy: Always
      imagePullSecrets:
        - name: registry-creds
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
---
kind: Service
apiVersion: v1
metadata:
  name: kaniko-service
  namespace: default
spec:
  ports:
    - protocol: TCP
      port: 80
  selector:
    app: kaniko-deployment
  type: NodePort
---
EOF
```

5. Apply the deployment file to kubernetes.
```
kubectl apply -f deployment.yaml
```

6. Find your URL by utilizing a built in minikube command & open your new website hosted on kubernetes.
```
minikube service kaniko-service --url
```

7. Go to your website and see your live project on a container in kubernetes.
